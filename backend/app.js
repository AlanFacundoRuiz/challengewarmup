const express = require('express');

const apiRouter = require('./routes/api');

const app = express();

app.set('port', process.env.PORT || 4000);

app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));

app.use('/api', apiRouter);

app.listen(app.get('port'), () => {
  console.log('Server on port', app.get('port'));
});