const express = require('express');
const router = express.Router();
const postsModel = require('../../models/postsModel');
const response = require('../../models/response');

router.get('/', async (req, res) => {
    const posts = await postsModel.getAllPosts();
    res.json(posts);
});

router.get('/:id', async (req, res) => {
    const id = req.params.id;
    const post = await postsModel.getPost(id);
    if (post.length > 0) {
        res.json(post)
    }
    response.error(res, 404)
});

router.post('/', async (req, res) => {
    try {
        const data = req.body;
        const imgLink = req.body.img;
        const regexImg = /([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png))/i;
        const validateImg = regexImg.test(imgLink);
        const emptyData = Object.values(data).some(value => value === '');
        if (!emptyData && validateImg) {
            await postsModel.createPost(data)
            response.success(res, 'Successfully created!', '', 201)
        } else
            response.error(res, 400)
    } catch (err) {
        response.error(res, 500)
    }
});

router.patch('/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const data = req.body;
        const result = await postsModel.updatePost(id, data);
        if (result[0]) {
            response.success(res, 'Successfully updated!')
        } else {
            response.error(res, 404)
        }
    } catch (err) {
        response.error(res, 500)
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const result = await postsModel.deletePost(id);
        if (result) {
            response.success(res, 'Successfully deleted!')
        } else {
            response.error(res, 404)
        }
    } catch (err) {
        response.error(res, 500)
    }
});

module.exports = router;