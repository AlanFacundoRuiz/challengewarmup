const Sequelize = require('sequelize');

const PostModel = require('../models/databaseModel');

const sequelize = new Sequelize('prueba', 'root', 'alanruiz95', {
    host: 'localhost',
    dialect: 'mysql'
});

const Post = PostModel(sequelize, Sequelize);

sequelize.sync({
        force: false
    })
    .then(() => {
        console.log('Tables Sync!');
    });

module.exports = {
    Post
};