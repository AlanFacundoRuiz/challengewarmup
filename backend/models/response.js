const success = (res, action, message, status) => {
    res.status(status || 200).json({
        [action]: message || "Ok"
    })
};

const error = (res, status) => {
    res.status(status).json({
        error: status
    })
};


module.exports = {
    success,
    error
}