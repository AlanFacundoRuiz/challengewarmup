module.exports = (sequelize, type) => {
    return sequelize.define('post', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        title: type.STRING,
        content: type.TEXT,
        img: type.STRING,
        category: type.STRING,
        datatime: type.TEXT
    })
}