const { Post } = require('../database/db');

const getAllPosts = async () => {
    const posts = await Post.findAll();
    return posts
}

const getPost = async (id) => {
    const post = await Post.findAll({
        where: {
            id
        }
    });
    return post
};

const createPost = async (data) => {
    const createPost = await Post.create(data);
    return createPost
};

const updatePost = async (id, data) => {
    const updatePost = await Post.update(data, {
        where: {
            id
        }
    });
    return updatePost
};


const deletePost = async (id) => {
    const deletePost = await Post.destroy({
        where: {
            id
        }
    });
    return deletePost
};

module.exports = {
    getAllPosts,
    getPost,
    createPost,
    updatePost,
    deletePost
}